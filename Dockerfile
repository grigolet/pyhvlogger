FROM ubuntu:latest

WORKDIR /app
RUN apt-get update && \
    apt-get install -y python3 git python3-pip && \
    pip3 install influxdb git+https://gitlab.cern.ch/grigolet/pyhvwrapper.git

COPY configs.json /configs/configs.json
COPY main.py ./main.py
USER 1001
ENTRYPOINT [ "python3", "main.py" ]

