from influxdb import InfluxDBClient
import json
from pyhvwrapper.HVWrapper import HV
from threading import Thread
import time
import os

configs = json.load(open('/configs/configs.json'))
hv_modules = {}
client = InfluxDBClient(**configs['db'])


def write(hv, module, slot_num, channel, name, log_params):
    # Check if status_code is enabled
    fields = {param: hv.get_param(slot_num, channel, param)[channel]
              for param in log_params if param != 'Status_code'}
    if 'Status_code' in log_params:
        fields['status_code'] = hv.get_param(
            slot_num, channel, 'Status', return_code=True)[channel]
    point = [{
        "measurement": "hv",
        "tags": {
            "module": module,
            "slot": slot_num,
            "channel": channel,
            "channel_name": name
        },
        "fields": fields
    }]
    success = client.write_points(point)


def log_hv(configs, time_resolution, log_params):
    # First, connect to the client
    module = configs['module_name']
    hv = HV(connection_arg=configs['connection_arg'],
            sys_type=configs['sys_type'],
            user=configs['user'],
            password=configs['password'],
            link_type=configs['link_type'])
    result = hv.connect()
    print(f'Connection to {configs["module_name"]}: {result}')
    # Start logging
    while (True):
        for slot_configs in configs['slots']:
            slot_num = slot_configs['number']
            channels = slot_configs['channels']
            channel_names = hv.get_ch_name(slot_num, channels)
            for channel, name in channel_names.items():
                write(hv, module, slot_num, channel, name, log_params)

        time.sleep(time_resolution)


if __name__ == "__main__":
    # Connect to HV client for each entry
    time_res = configs['logging']['time_resolution']
    log_params = configs['logging']['params']
    for hv_module_configs in configs['hv']:
        print('Reading configs: ', hv_module_configs)
        Thread(target=log_hv, args=(hv_module_configs,
                                    time_res, log_params)).start()
