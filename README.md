# pyhvlogger

This project is used to build a docker image that can log into an influxdb database the value from an hv module

```bash
$ docker build . -t pyhvlogger:latest 
```

```bash
$ docker run -v $(pwd)/main.py:/app/main.py -v $(pwd)/configs.json:/configs/configs.json pyhvlogger:latest
```

Example of `configs.json`:
```json
{
    "db": {
        "host": "url.influxdb.com",
        "username": "username",
        "password": "password",
        "database": "database",
        "port": 8081,
        "ssl": true,
        "verify_ssl": false
    },
    "logging": {
        "time_resolution": 2,
        "params": ["V0Set", "IMon", "Status"]
    },
    "hv": [
        {
            "module_name": "caenmodulename",
            "sys_type": 3,
            "user": "admin",
            "password": "admin",
            "connection_arg": "0.0.0.0",
            "link_type": 0,
            "slots": [{
                "number": 0,
                "channels": [0, 1]
            }]
        }
    ]
}
```